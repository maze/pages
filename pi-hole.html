<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us" xml:lang="en-us">
  <head>
    <title>Setting up a Pi-hole on my Raspberry Pi</title>
    <meta name="owner"       content="Michael Zeevi">
    <meta name="designer"    content="Michael Zeevi">
    <meta name="author"      content="Michael Zeevi">
    <meta name="copyright"   content="Michael Zeevi">
    <meta name="date"        content="2021-05-30">
    <meta name="revised"     content="2021-05-30">
    <meta name="keywords"    content="pi-hole, raspberry-pi, dns">
    <meta name="generator"   content="pandoc"> <!-- template by Michael Zeevi -->
    <meta name="viewport"    content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="language"    content=”en-us”>
    <meta http-equiv="content-language" content=”en-us”>
    <meta http-equiv="x-ua-compatible"  content="ie=edge">
    <meta charset="utf-8">
    <link rel="stylesheet" href="res/styles.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>

  <body>
    <header class="wrapper">
      <h1 id="site-title"><a href="index.html">maze88.dev</a></h1>

      <nav>
        <ul id="site-menu">
          <li><a href="index.html">Home</a></li>
          <li><a href="blog.html">Tech Blog</a></li>
          <li><a href="links.html">Links</a></li>
          <li><a href="photography.html">Photography</a></li>
        </ul>
      </nav>
    </header>

    <div id="colorscheme">
      <a id="colorscheme-toggle">
        <i class="fas fa-fw fa-moon"></i><i class="fas fa-fw fa-toggle-on" id="colorscheme-toggle-switch"></i><i class="fas fa-fw fa-sun"></i>
      </a>
      <script src="res/colorscheme.js"></script>
    </div>

    <main>
      <hr class="hidden-on-normal-displays">

      <header>
        <h1 id="content-title">Setting up a Pi-hole on my Raspberry
Pi</h1>
        <p class="content-header author">Michael Zeevi</p>
        <p class="content-header date"><time datetime="2021-05-30">2021-05-30</time></p>
      </header>

<h2 id="intro">Intro</h2>
<p>Today’s personal endeavor was setting up a <a
href="https://pi-hole.net/">Pi-hole</a> to block unwanted content for
all devices connected to my home network.</p>
<h2 id="how-it-works">How it works</h2>
<p>For anybody unfamiliar, I’ll briefly explain how a Pi-hole works:</p>
<p>The Pi-hole acts as a <em>sole</em> (single) local DNS server,
configured with a blacklist of domains which are known for serving ads
and other unwanted content (such as trackers). This way, whenever an
application (such as a web browser, or even a game on one’s smartphone)
needs to fetch external content, then the process starts by attempting
to resolve the serving domain; if the domain is in the Pi-hole’s
blacklist then the Pi-hole <em>doesn’t</em> return the content. The
importance of a Pi-hole being the <em>sole</em> DNS server on the
network is so that when queries for unwanted content are blocked, there
should <em>not</em> be an alternative DNS server to fall-back on, which
would succeed at resolving the queries for unwanted content.</p>
<h2 id="deployment">Deployment</h2>
<p>I deployed <em>my</em> Pi-hole server as a Docker container, running
on a <a
href="https://www.raspberrypi.org/products/raspberry-pi-4-model-b/">Raspberry
Pi 4</a>. Specific instructions about this can be found on the <a
href="https://hub.docker.com/r/pihole/pihole">Pi-hole page at
DockerHub</a>. The <code>docker-compose.yaml</code> file contains:</p>
<pre><code>version: &quot;3&quot;
services:
  pihole:
    container_name: pihole
    hostname: &#39;pi.hole&#39;
    image: pihole/pihole:v5.8.1-armhf-buster
    network_mode: host
    environment:
      TZ: &#39;Israel&#39;
      PIHOLE_DNS_: &#39;1.1.1.1;1.0.0.1&#39;
      DNSSEC: &#39;true&#39;
      VIRTUAL_HOST: &#39;pi.hole&#39;
    volumes:
      - &#39;./etc-pihole/:/etc/pihole/&#39;
      - &#39;./etc-dnsmasq.d/:/etc/dnsmasq.d/&#39;
    cap_add:
      - NET_ADMIN
    restart: unless-stopped</code></pre>
<h2 id="router-configuration-dhcp-and-dns">Router configuration (DHCP
and DNS)</h2>
<p>In my router I reserved the Raspberry Pi’s IP address’ DHCP lease (so
that it wouldn’t have the chance to change in case of a restart, etc.),
and configured the router’s DNS nameserver to use the Raspberry Pi,
which serves the Pi-hole. Note that the <em>Secondary DNS</em> field -
which must remain empty!</p>
<figure>
<img src="res/pi-hole/router-dhcp-reservation-lease.png"
alt="Router DHCP reservation lease" />
<figcaption aria-hidden="true">Router DHCP reservation
lease</figcaption>
</figure>
<figure>
<img src="res/pi-hole/router-dns-config.png"
alt="Router DNS configuration" />
<figcaption aria-hidden="true">Router DNS configuration</figcaption>
</figure>
<h2 id="pi-hole-configuration-local-dns-record">Pi-hole configuration
(local DNS record)</h2>
<p>In the Pi-hole I optionally chose to configure a local DNS record to
map the domain name <a href="http://pi.hole/">pi.hole</a> to its
[reserved] IP address. This allows me to access the Pi-hole’s front end
dashboard (which it exposes automatically) via a comfortable domain
name, instead of its IP address.</p>
<figure>
<img src="res/pi-hole/address-bar.png" alt="Pi-hole address bar" />
<figcaption aria-hidden="true">Pi-hole address bar</figcaption>
</figure>
<figure>
<img src="res/pi-hole/dashboard.png" alt="Pi-hole dashboard" />
<figcaption aria-hidden="true">Pi-hole dashboard</figcaption>
</figure>
<h2 id="conclusion">Conclusion</h2>
<p>In conclusion, this is a neat project which involves a nice handful
of network and internet theory, bundled into an elegant solution for an
everyday problem.</p>
<p>There is one major caveat with this solution, in regards of blocking
ads: it doesn’t work for ads in YouTube videos. The reason for this is
because the ads are served from the <em>same</em> domains as the actual
video content. Therefore, if one’s a frequent consumer of YouTube, then
they should consider using a browser based ad-blocker plugin in addition
to a Pi-hole.</p>

      <hr class="hidden-on-normal-displays">
    </main>

    <footer>
      <address>
        <ul id="social">
          <li><a rel="author" target="_blank" href="mailto:michael.zeevi@fastmail.net" title="E-mail: Michael Zeevi"           ><i class="fas fa-fw fa-envelope"   ></i><div class="hidden-on-tiny-displays"> E-mail</div></a></li>
          <li><a rel="author" target="_blank" href="https://fosstodon.org/@maze"       title="Mastodon: maze@fosstodon.org"    ><i class="fab fa-fw fa-mastodon"   ></i><div class="hidden-on-tiny-displays"> Mastodon</div></a></li>
          <li><a rel="author" target="_blank" href="https://pixelfed.social/@maze88"   title="Pixelfed: maze88@pixelfed.social"><i class="fas fa-fw fa-camera"     ></i><div class="hidden-on-tiny-displays"> Pixelfed</div></a></li>
          <li><a rel="author" target="_blank" href="https://codeberg.org/maze"         title="Codeberg: maze"                  ><i class="fas fa-fw fa-code-branch"></i><div class="hidden-on-tiny-displays"> Codeberg</div></a></li>
          <li><a rel="author" target="_blank" href="res/pgp.asc"                       title="PGP: Public key block"           ><i class="fas fa-fw fa-fingerprint"></i><div class="hidden-on-tiny-displays"> PGP</div></a></li>
          <li><a rel="author" target="_blank" href="https://keyoxide.org/hkp/a04876190823b1cc383e882d2458479e16ef8831" title="Keyoxide"><i class="fas fa-fw fa-key"></i><div class="hidden-on-tiny-displays"> Keyoxide</div></a></li>
          <li><a rel="author" target="_blank" href="https://maze.omg.lol"              title="omg.lol page"                    ><i class="fas fa-fw fa-heart"      ></i><div class="hidden-on-tiny-displays"> omg.lol</div></a></li>
        </ul>
      </address>

      <p id="donate">
        If you like what you see, feel free to <a target="_blank" href="https://liberapay.com/maze/donate">donate via Liberapay</a>.
      </p>

      <p id="license" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/">
        <a href="index.html" property="dct:title" rel="cc:attributionURL">maze88.dev</a> by
        <a target="_blank" href="mailto:michael.zeevi@fastmail.net" property="cc:attributionName" rel="cc:attributionURL dct:creator">Michael Zeevi</a> is licensed under
        <a target="_blank" href="http://creativecommons.org/licenses/by-sa/4.0"                   rel="license noopener noreferrer">CC BY-SA 4.0</a>.
      </p>
    </footer>
  </body>
</html>
