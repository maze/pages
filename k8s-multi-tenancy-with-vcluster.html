<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <title>Multi-tenancy Kubernetes with Virtual Clusters</title>
    <meta name="owner"       content="Michael Zeevi">
    <meta name="designer"    content="Michael Zeevi">
    <meta name="author"      content="Michael Zeevi">
    <meta name="copyright"   content="Michael Zeevi">
    <meta name="date"        content="2023-09-26">
    <meta name="revised"     content="2023-09-26">
    <meta name="keywords"    content="kubernetes, multitenancy, vcluster, namespacing, multicluster, virtualization, k3s, k0s">
    <meta name="generator"   content="pandoc"> <!-- template by Michael Zeevi -->
    <meta name="viewport"    content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta http-equiv="x-ua-compatible"  content="ie=edge">
    <meta charset="utf-8">
    <link rel="stylesheet" href="res/styles.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>

  <body>
    <header class="wrapper">
      <h1 id="site-title"><a href="index.html">maze88.dev</a></h1>

      <nav>
        <ul id="site-menu">
          <li><a href="index.html">Home</a></li>
          <li><a href="blog.html">Tech Blog</a></li>
          <li><a href="links.html">Links</a></li>
          <li><a href="photography.html">Photography</a></li>
        </ul>
      </nav>
    </header>

    <div id="colorscheme">
      <a id="colorscheme-toggle">
        <i class="fas fa-fw fa-moon"></i><i class="fas fa-fw fa-toggle-on" id="colorscheme-toggle-switch"></i><i class="fas fa-fw fa-sun"></i>
      </a>
      <script src="res/colorscheme.js"></script>
    </div>

    <main>
      <hr class="hidden-on-normal-displays">

      <header>
        <h1 id="content-title">Multi-tenancy Kubernetes with Virtual
Clusters</h1>
        <p class="content-header author">Michael Zeevi</p>
        <p class="content-header date"><time datetime="2023-09-26">2023-09-26</time></p>
      </header>

<style>
  .positive {
    color: #ae5;
  }
  .negative {
    color: #f77;
  }
</style>
<p>In this article you will be introduced to the realm of virtual
clusters in Kubernetes. We shall explore an innovative solution that not
only aids Kubernetes administrators and operators, but also benefits
developers and consumers, and hopefully also inspires DevOps enthusiasts
alike. By delving into the design and architecture of virtual clusters,
we’ll uncover how this solution caters to the diverse scenarios that
warrant multi-tenancy within Kubernetes.</p>
<h2 id="multi-tenancy-introduction">Multi-tenancy introduction</h2>
<p>In the realm of Kubernetes administration, the term multi-tenancy may
sometimes come up. A ‘tenant’ here refers to any entity utilizing the
cluster, be it an individual like the cluster administrator, a team like
a service’s developers, or even a customer utilizing a semi-managed
product. The necessity for isolation between these tenants often arises
to address concerns such as authorization &amp; access management,
communication security, data privacy, stability within specific
environments (production, etc.), maintaining order, and the imposition
of custom resource limits per tenant.</p>
<p>Some example use cases that may utilize a multi-tenancy approach in
Kubernetes are:</p>
<ul>
<li>Management of distinct environments such as <em>staging</em> or
<em>testing</em>.</li>
<li>Providing individual developers (or teams) with separate
environments for their work.</li>
<li>When a company provides a managed product that is
Kubernetes-deployable, having a segregated instance of the application
for each customer ensures a tailored experience.</li>
<li>As a teaching instrument for onboarding or training labs, providing
isolated sandbox environments.</li>
</ul>
<h2
id="traditional-multi-tenancy-approaches-and-their-drawbacks">Traditional
multi-tenancy approaches (and their drawbacks)</h2>
<p>In considering traditional approaches to multi-tenancy in Kubernetes,
it’s crucial to weigh their advantages and drawbacks.</p>
<p>One method that instantly comes to mind involves utilizing multiple
clusters. However, this approach is highly expensive due to the need for
extensive [cloud] infrastructure, and because the process of spinning up
new clusters is notably slow (often many tens of minutes). Moreover,
managing this plethora of clusters demands a significant amount of
effort.</p>
<p>Alternatively, the second method that comes to mind is Kubernetes
Namespaces. However, this approach also presents its limitations - an
application within a namespace is restricted to that namespace alone,
preventing creation of cluster-wide (non-namespaced) resources, like
ClusterRoles or Custom Resource Definitions (CRDs). Furthermore,
Kubernetes namespaces are designed for providing logical organization,
and may fall short in delivering stringent security isolation (yes, RBAC
can indeed restrict access by namespace, but then one literally has only
a <em>single</em> layer of security).</p>
<h2 id="enter-vcluster">Enter VCluster</h2>
<p><em>VCluster</em> (Virtual Clusters) by <a
href="https://loft.sh"><em>Loft</em></a> allows spinning up virtual
clusters within an existing host Kubernetes cluster. These virtual
clusters function as fully operational Kubernetes clusters. Similarly to
virtual machines, virtual clusters utilize/share a single host cluster’s
resources into distinct virtual clusters, effectively accommodating
multiple tenants.</p>
<p>Each virtual cluster operates autonomously, complete with its
dedicated control plane and datastore (equivalent to <em>etcd</em>),
encapsulating the essence of a standalone Kubernetes environment. The
workloads and services within a virtual cluster appear entirely normal,
whilst, under the hood, they are actually scheduled on the host cluster
and into a single designated namespace. This arrangement preserves
flexibility (to use multiple namespaces) within the virtual cluster
while maintaining order within the host cluster.</p>
<p>VCluster’s CLI utility may be installed with a simple
<code>curl</code> one-liner (see <a
href="https://www.vcluster.com/docs/getting-started/setup%23download-vcluster-cli">the
installation instructions in VCluster’s
<em>getting-started</em> guide</a>) or -if using a Mac- with
<code>brew</code>.</p>
<p>By the way, all of this is <a
href="https://github.com/loft-sh/vcluster">open source</a>..!</p>
<h2 id="basic-usage">Basic Usage</h2>
<p>This article’s focus is on the concepts and theory around and behind
virtual clusters. In order to get a taste of VCluster’s practical
aspects, refer to its <a
href="https://www.vcluster.com/docs/quickstart">quickstart guide
available in the VCluster documentation</a>. Additionally, the VCluster
documentation also provides many details about various aspects of the
administration and operation of VClusters, such as the Operator guide or
<a href="https://www.vcluster.com/docs/config-reference">Configuration
reference</a>.</p>
<h2 id="comparison-to-traditional-multi-tenancy-approaches">Comparison
to traditional multi-tenancy approaches</h2>
<p>The following table (from VCluster’s official documentation) clearly
summarizes how VCluster delivers the best of both worlds, when providing
multi-tenancy approaches:</p>
<table>
<colgroup>
<col style="width: 21%" />
<col style="width: 25%" />
<col style="width: 26%" />
<col style="width: 26%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: center;"></th>
<th><em>Namespace</em><br>for each tenant</th>
<th><em>VCluster</em></th>
<th><em>Cluster</em><br>for each tenant</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: center;">Isolation</td>
<td><span class="negative">✗</span> very weak</td>
<td><span class="positive">✔</span> <strong>strong</strong></td>
<td><span class="positive">✔</span> <strong>very strong</strong></td>
</tr>
<tr class="even">
<td style="text-align: center;">Access for tenants</td>
<td><span class="negative">✗</span> very restricted</td>
<td><span class="positive">✔</span> <strong>vcluster admin</strong></td>
<td><span class="positive">✔</span> <strong>cluster admin</strong></td>
</tr>
<tr class="odd">
<td style="text-align: center;">Cost</td>
<td><span class="positive">✔</span> <strong>very cheap</strong></td>
<td><span class="positive">✔</span> <strong>cheap</strong></td>
<td><span class="negative">✗</span> expensive</td>
</tr>
<tr class="even">
<td style="text-align: center;">Resource sharing</td>
<td><span class="positive">✔</span> <strong>easy</strong></td>
<td><span class="positive">✔</span> <strong>easy</strong></td>
<td><span class="negative">✗</span> very hard</td>
</tr>
<tr class="odd">
<td style="text-align: center;">Overhead</td>
<td><span class="positive">✔</span> <strong>very low</strong></td>
<td><span class="positive">✔</span> <strong>very low</strong></td>
<td><span class="negative">✗</span> very high</td>
</tr>
</tbody>
</table>
<p>(table data source: <a
href="https://www.vcluster.com">https://www.vcluster.com</a>)</p>
<h2 id="vcluster-provisioning-methods">VCluster provisioning
methods</h2>
<p>Provisioning virtual clusters (VClusters) within Kubernetes can be
achieved in various methods:</p>
<h3 id="cli">CLI</h3>
<p>The VCluster CLI provides an imperative approach to provisioning,
allowing for rapid outcomes. The virtual clusters may receive basic
configuration through various CLI flags, or in-depth configuration by
passing a configuration file. The configuration files are actually just
Helm <code>values.yaml</code> files, this convenient feature is due to
the VCluster CLI actually just being a wrapper of the Helm
implementation. The VCLuster CLI is most convenient for management
tasks, with commands such as <code>vcluster ls</code> or
<code>vcluster connect ...</code>.</p>
<h3 id="helm">Helm</h3>
<p>Well known to many DevOps engineers and Kubernetes administrators,
Helm offers a familiar approach to declaratively provisioning virtual
clusters via <a
href="https://artifacthub.io/packages/helm/loft/vcluster">the official
VCluster Helm chart</a>. Besides streamlining the standard operations,
Helm allows one to operate VClusters in a standalone manner, with no
additional dependencies (no need even for the CLI!), and is highly
suited for provisioning VClusters via GitOps practices.</p>
<h3 id="terraform">Terraform</h3>
<p>VClusters may also be provisioned using Terraform via a dedicated
Terraform provider. This method is ideal for operators who rely on
Terraform and want to include their virtual clusters as part of their
infrastructure as code. More details can be explored through <a
href="https://registry.terraform.io/providers/loft-sh/loft/latest/docs">the
VCluster Terraform provider’s documentation</a>.</p>
<p>Between these provisioning methods, administrators can easily choose
the approach that aligns best with their workflow, ensuring an efficient
and tailored provisioning process for VClusters within their Kubernetes
environment.</p>
<h2 id="architecture">Architecture</h2>
<p>A pillar in VCluster’s architecture design involves categorizing
Kubernetes resource kinds as <em>high-level</em> and
<em>low-level</em>:</p>
<ul>
<li><strong>High-level resources</strong> - Reside only within the
virtual cluster, and respectively, their state is registered only in the
virtual cluster’s data store. Some examples of high-level resources are
Deployments, StatefulSets, Ingresses, ServiceAccounts, CRDs, or
Jobs.</li>
<li><strong>Low-level resources</strong> - Appear normally within the
virtual cluster, however, they are also synchronized into the
<em>host</em> cluster (more on this below) - where they are
organized/isolated into a single host Namespace.</li>
</ul>
<p>Within the host cluster VClusters are deployed as a StatefulSet,
which manages a single Pod comprised of two containers:</p>
<ol type="1">
<li><strong>The Control Plane</strong> - Which bundles the standard
Kubernetes components such as the API server, controller manager, and
the data store (classically [etcd] or SQLite) in a singular Kubernetes
distribution (by default <em>k3s</em>).</li>
<li><strong>The Syncer</strong> - Which is the workhorse behind
VCluster. The Syncer’s role mainly comprises of copying low-level
resources from the virtual cluster to the underlying host cluster.</li>
</ol>
<p>Furthermore, due to their streamlined design, VClusters even provide
the flexibility to support nested VClusters.</p>
<figure>
<img src="../res/vcluster/architecture-dark.png"
alt="(based on diagram from: https://www.vcluster.com/docs/architecture/basics)" />
<figcaption aria-hidden="true">(based on diagram from: <a
href="https://www.vcluster.com/docs/architecture/basics">https://www.vcluster.com/docs/architecture/basics</a>)</figcaption>
</figure>
<h3 id="distributions">Distributions</h3>
<p>When deploying a VCluster, one may use any of the certified
Kubernetes distributions. The default choice is <a
href="https://github.com/k3s-io/k3s">k3s</a> (known for its small
footprint and wide adoption). Additional possibilities include <a
href="https://github.com/k0sproject/k0s">k0s</a> or <a
href="https://anywhere.eks.amazonaws.com">EKS anywhere</a>, and, of
course, <em>Vanilla k8s</em> (the CNCF’s official version).</p>
<h3 id="resource-names-structure">Resource names’ structure</h3>
<p>Within the virtual cluster, Kubernetes resources are named normally
(<code>&lt;resource name in vcluster&gt;</code>) and may reside in any
desired namespace. This provides the expected and conventional behavior
for its tenants (users and administrators alike).</p>
<p>In the host cluster, where the <em>low-level</em> resources are
synchronized, they reside in a single namespace and are automatically
named (by the Syncer container) using the following structure:
<code>&lt;resource name in vcluster&gt;-x-&lt;namespace in vcluster&gt;-x-&lt;name of vcluster&gt;</code>.
Besides preventing resource name conflicts (e.g. same name but different
namespaces, within the virtual cluster), this format also helps maintain
order, and allows simple identification of resources for management by
host cluster administrators.</p>
<h2 id="cleanup">Cleanup</h2>
<p>Deleting a VCluster (or even simply deleting its namespace from the
host cluster) will always be possible and graceful, with no negative
impacts on the host cluster (no “zombie” resources stuck in a
termination state, etc.). This is smartly implemented by:</p>
<ul>
<li>VClusters do not affect the host cluster’s control plane nor add any
server-side elements.</li>
<li>The VClusters’ StatefulSets and any other resources, all have
appropriate owner references, thus if the VCluster is deleted,
everything that belongs to it will be automatically deleted by
Kubernetes as well (via a mechanism similar to how deleted Deployments
clean up their Pods).</li>
</ul>
<h2 id="caveats">Caveats</h2>
<p>For the most part, VCluster works very well due to its simplicity and
should cater well to many basic and moderate use cases. However, under
certain more advanced conditions, some complications or issues may
arise. The following list is not exhaustive, but covers several examples
and some experience from my work with it:</p>
<ul>
<li><strong>StorageClasses</strong> - Despite dynamic storage
provisioning and persistence functioning fine, one may notice that - by
default - no StorageClass resources exist in the VCluster. A workaround
to this would be to use <a
href="https://www.vcluster.com/docs/architecture/synced-resources">custom
configuration for the Syncer</a> by setting
<code>sync.hoststorageclasses.enabled=true</code>.</li>
<li><strong>DaemonSets</strong> - By default, VCluster will
create/display “fake nodes” because it has no RBAC permissions (by
default) to view the real nodes of the underlying host cluster. This may
lead to misbehavior of DaemonSets, such as preventing scaling-in nodes.
A workaround to this would be to <a
href="https://www.vcluster.com/docs/architecture/nodes%23node-syncing-modes">configure
the synchronization of Nodes and set an appropriate mode</a> (such as
<code>sync.nodes.enabled=true</code> &amp;
<code>sync.nodes.syncAllNodes=true</code>).</li>
<li><strong>Network Policies</strong> By default, VCluster ignores
Network Policies. However, one can enable synchronization of them to the
host cluster by setting <code>sync.networkpolicies.enabled=true</code>,
thus achieving the desired traffic behavior. It should go without saying
that this feature relies on the support of Network Policies by the host
cluster’s CNI.</li>
<li><strong>Pod Schedulers</strong> - In the niche cases one wishes to
<a
href="https://www.vcluster.com/docs/architecture/scheduling%23separate-vcluster-scheduler">use
a custom Pod Scheduler</a>, it’s essential to recognize that they are
not supported by all the Kubernetes distributions available in VCluster.
This is due to the fact that Pods are considered low-level resources,
and the actual scheduling of them is managed by the host cluster’s
scheduler.</li>
</ul>
<p>Understanding and navigating these caveats will empower users to make
informed decisions and effectively leverage VClusters while considering
their specific use cases and requirements.</p>
<h2 id="conclusion">Conclusion</h2>
<p>In conclusion, we familiarized ourselves with the concept of
multi-tenancy in Kubernetes, and touched on various scenarios that call
for multi-tenancy. We compared traditional multi-tenancy approaches in
Kubernetes with VCluster, and learned how VCluster provides us with a
“best of both worlds” solution.</p>
<p>We explored VClusters from a Kubernetes cluster administrator’s
perspective, focusing mainly on theoretical concepts and architectural
aspects, such as the way VCluster handles high and low-level resources -
synchronizing only the low-level resources to the host cluster. We
addressed several caveats and discussed the nature of their causes and
how to potentially work around them, giving us a peek into the finer
configuration that is available when provisioning VClusters.</p>
<p>So, if you were in the search for a streamlined multi-tenancy
solution within Kubernetes, are a DevOps enthusiast, or even simply a
Kubernetes administrator (or all of them) - then this overview article
may have introduced you to your next big tool, or simply provide you
with a handy utility for toying around with sandbox environments. Either
way, VClusters proves to be a flexible and powerful tool, empowering
users to optimize their multi-tenant Kubernetes environments, fostering
security, isolation, and streamlined management.</p>
<h2 id="sources-additional-info">Sources &amp; additional info</h2>
<ul>
<li><a href="https://www.vcluster.com/docs/">VCluster
documentation</a> - A great place to get started or dive into the
advanced.</li>
<li><a
href="https://artifacthub.io/packages/helm/loft/vcluster">VCluster Helm
chart</a> - More than enough to get up and running without installing
anything else.</li>
<li><a
href="https://registry.terraform.io/providers/loft-sh/loft/latest/docs">VCluster
Terraform provider</a> - For managing via Terraform.</li>
<li><a href="https://loft.sh">Loft</a> - A non-free software that
assists in managing VClusters by web GUI or CLI, creating templates,
integrating with SSO, and more.</li>
<li><a href="https://slack.loft.sh">Loft Slack</a> - VCluster official
community Slack.</li>
</ul>

      <hr class="hidden-on-normal-displays">
    </main>

    <footer>
      <address>
        <ul id="social">
          <li><a rel="author" target="_blank" href="mailto:michael.zeevi@fastmail.net" title="E-mail: Michael Zeevi"           ><i class="fas fa-fw fa-envelope"   ></i><div class="hidden-on-tiny-displays"> E-mail</div></a></li>
          <li><a rel="author" target="_blank" href="https://fosstodon.org/@maze"       title="Mastodon: maze@fosstodon.org"    ><i class="fab fa-fw fa-mastodon"   ></i><div class="hidden-on-tiny-displays"> Mastodon</div></a></li>
          <li><a rel="author" target="_blank" href="https://pixelfed.social/@maze88"   title="Pixelfed: maze88@pixelfed.social"><i class="fas fa-fw fa-camera"     ></i><div class="hidden-on-tiny-displays"> Pixelfed</div></a></li>
          <li><a rel="author" target="_blank" href="https://codeberg.org/maze"         title="Codeberg: maze"                  ><i class="fas fa-fw fa-code-branch"></i><div class="hidden-on-tiny-displays"> Codeberg</div></a></li>
          <li><a rel="author" target="_blank" href="res/pgp.asc"                       title="PGP: Public key block"           ><i class="fas fa-fw fa-fingerprint"></i><div class="hidden-on-tiny-displays"> PGP</div></a></li>
          <li><a rel="author" target="_blank" href="https://keyoxide.org/hkp/a04876190823b1cc383e882d2458479e16ef8831" title="Keyoxide"><i class="fas fa-fw fa-key"></i><div class="hidden-on-tiny-displays"> Keyoxide</div></a></li>
          <li><a rel="author" target="_blank" href="https://maze.omg.lol"              title="omg.lol page"                    ><i class="fas fa-fw fa-heart"      ></i><div class="hidden-on-tiny-displays"> omg.lol</div></a></li>
        </ul>
      </address>

      <p id="donate">
        If you like what you see, feel free to <a target="_blank" href="https://liberapay.com/maze/donate">donate via Liberapay</a>.
      </p>

      <p id="license" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/">
        <a href="index.html" property="dct:title" rel="cc:attributionURL">maze88.dev</a> by
        <a target="_blank" href="mailto:michael.zeevi@fastmail.net" property="cc:attributionName" rel="cc:attributionURL dct:creator">Michael Zeevi</a> is licensed under
        <a target="_blank" href="http://creativecommons.org/licenses/by-sa/4.0"                   rel="license noopener noreferrer">CC BY-SA 4.0</a>.
      </p>
    </footer>
  </body>
</html>
