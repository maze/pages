# maze88.dev
- This repository contains the static files for my website/blog: https://maze88.dev

- It is hosted using [Codeberg pages](https://codeberg.page).

- The content is generated using my SSG, which can be found in my [_pages-ssg_ repository](https://codeberg.org/maze/pages-ssg), along with the source files of my content.
