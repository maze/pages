<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en-us" xml:lang="en-us">
  <head>
    <title>No Linux? Windows Subsystem for Linux!</title>
    <meta name="owner"       content="Michael Zeevi">
    <meta name="designer"    content="Michael Zeevi">
    <meta name="author"      content="Michael Zeevi">
    <meta name="copyright"   content="Michael Zeevi">
    <meta name="date"        content="2021-08-05">
    <meta name="revised"     content="2021-08-05">
    <meta name="keywords"    content="wsl, linux, bash, bashrc, virtualization">
    <meta name="generator"   content="pandoc"> <!-- template by Michael Zeevi -->
    <meta name="viewport"    content="width=device-width, initial-scale=1.0, user-scalable=yes">
    <meta name="language"    content=”en-us”>
    <meta http-equiv="content-language" content=”en-us”>
    <meta http-equiv="x-ua-compatible"  content="ie=edge">
    <meta charset="utf-8">
    <link rel="stylesheet" href="res/styles.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
  </head>

  <body>
    <header class="wrapper">
      <h1 id="site-title"><a href="index.html">maze88.dev</a></h1>

      <nav>
        <ul id="site-menu">
          <li><a href="index.html">Home</a></li>
          <li><a href="blog.html">Tech Blog</a></li>
          <li><a href="links.html">Links</a></li>
          <li><a href="photography.html">Photography</a></li>
        </ul>
      </nav>
    </header>

    <div id="colorscheme">
      <a id="colorscheme-toggle">
        <i class="fas fa-fw fa-moon"></i><i class="fas fa-fw fa-toggle-on" id="colorscheme-toggle-switch"></i><i class="fas fa-fw fa-sun"></i>
      </a>
      <script src="res/colorscheme.js"></script>
    </div>

    <main>
      <hr class="hidden-on-normal-displays">

      <header>
        <h1 id="content-title">No Linux? Windows Subsystem for
Linux!</h1>
        <p class="content-header author">Michael Zeevi</p>
        <p class="content-header date"><time datetime="2021-08-05">2021-08-05</time></p>
      </header>

<h2 id="intro">Intro</h2>
<p>This post will focus on <em>Windows Subsystem for Linux</em> (WSL),
its advantages (over a conventional VM running Linux), setting it up and
some tips for getting started with it.</p>
<h2 id="background">Background</h2>
<p>Sometimes you want your Linux environment, but for security reasons
your workplace/company/customer enforces usage of standard issue
“uniform” Windows computers (not that Linux is not secure, just that IT
teams often choose to <em>not</em> manage multiple security tools &amp;
policies for different operating systems). In these cases we have
<strong>a few options</strong>...</p>
<p>One option is creating a “classic” Virtual Machine (using
<strong><em>Virtual Box</em></strong>, for example), reserving a portion
of your host system’s resources (memory, storage, etc.), and installing
a full Linux image (such as Ubuntu) on it.</p>
<p>Another option, is to use <strong>WSL</strong> with a Linux
distribution (such as Ubuntu) from the Microsoft Store, which we’ll
discuss below.</p>
<blockquote>
<p>Note: I won’t touch options like CygWin or MinGW, because they are
riddled with differences such as not actually supporting Linux packages
(instead they need them to go through modifications before even being
compiled).</p>
</blockquote>
<h2 id="advantages">Advantages</h2>
<p>Some of the advantages that make WSL standout:</p>
<ul>
<li>Has very low memory demands - it doesn’t “block” a portion of the
host system’s memory. Instead, it shares it - <em>similarly</em> to how
a Docker container behaves (this can be demonstrated by running
<code>free -h</code> in WSL and seeing the output list your host’s
<em>full</em> memory).</li>
<li>Uses a special VM tailored for its purpose and allowing it to be
very smooth, lightweight and responsive.</li>
<li>Natively integrates with Windows host - for example its filesystem
is automatically mounted and available from within the Linux
distribution off that bat (without any special configuration).</li>
<li>Natively <a
href="https://docs.docker.com/docker-for-windows/wsl/">integrates with
Docker</a> for Windows.</li>
</ul>
<h2 id="caveats">Caveats</h2>
<p>Nothing is perfect (not <em>even</em> Linux), definitely not WSL (it
runs on Windows after all..), so here are some slight disadvantages:</p>
<ul>
<li>Doesn’t support GUI based applications (hopefully you love the CLI
like I do!) out of the box (but it <a
href="https://docs.microsoft.com/en-us/windows/wsl/tutorials/gui-apps"><em>is</em>
possible with specific drivers</a>).</li>
<li>Cannot run 32-bit applications (i.e. relics of the past).</li>
</ul>
<h2 id="setup">Setup</h2>
<p>WSL is a Windows feature that must be enabled. Since it uses a VM
behind the scenes, one must also <a
href="https://www.google.com/search?q=enable+hardware+virtualization">enable
hardware <strong>virtualization</strong></a> in your computer’s
BIOS.</p>
<p>Setup steps (assuming virtualization is already enabled in BIOS):</p>
<ol type="1">
<li><p>Run <strong>Powershell</strong> as Administrator and input the
following commands:</p>
<pre><code>dism.exe /online /enable-feature /all /norestart /featurename:VirtualMachinePlatform
dism.exe /online /enable-feature /all /norestart /featurename:Microsoft-Windows-Subsystem-Linux</code></pre></li>
<li><p>Open the <em>Microsoft Store</em> (you can search for it from
<em>Start Menu</em>), search for and install your desired <strong>Linux
distribution</strong> (Debian or Ubuntu are solid choices) and
<strong>Windows Terminal</strong> (which is a more feature-rich client
than the default client - for example, it allows opening multiple
tabs).</p></li>
<li><p>Run Windows Terminal and go into <em>Settings</em>. Under
<em>Startup</em>, set <strong><em>Default profile</em></strong> to the
Linux distribution you got last step.</p></li>
<li><p>Still in <em>Settings</em>, under <em>Profiles</em> select your
Linux distribution and set the <strong><em>Starting
directory</em></strong> to
<code>//wsl$/&lt;YOUR_DISTRO_NAME&gt;/home/&lt;YOUR_USER_NAME&gt;</code>
(this will make WSL boot into your Linux home directory, insted of the
[default] Windows one).</p></li>
<li><p>The first time you launch your new WSL distribution it will run
its setup and prompt you for setting a <strong>root user</strong>
password, and creating your <strong>own user</strong>. Do this.</p></li>
</ol>
<h2 id="follow-up-actions-and-tips">Follow-up actions and tips</h2>
<p>After running your new WSL Linux distribution for the first time,
here are a few useful things to do:</p>
<ul>
<li><p>Install common packages normally. Here are some useful ones if
you are on Debian based distributions:</p>
<pre><code>sudo apt install -y \
  bash-completion \
  dnsutils \
  git \
  iputils-ping \
  iproute2 \
  jq \
  man-db \
  python3 \
  python3-pip \
  unzip</code></pre></li>
<li><p>Symbolically link the main folders from your Windows home
directory (which is case-sensitive) to your Linux home directory. I
suggest linking <em>Documents</em> and <em>Downloads</em>:</p>
<pre><code>ln -s /mnt/c/Users/${YOUR_WINDOWS_USERNAME}/Documents/ ~/Documents
ln -s /mnt/c/Users/${YOUR_WINDOWS_USERNAME}/Downloads/ ~/Downloads</code></pre>
<blockquote>
<p>Note the automatically mounted <code>C:</code> drive from Windows
under your Linux filesystem’s <code>/mnt/c/</code>.</p>
</blockquote></li>
<li><p>Configure your organization’s proxy (and exclude list), by
appending to the <code>/etc/environment</code> file (replace with your
organization’s proxy details):</p>
<pre><code>export HTTP_PROXY=http://your.organization.proxy:8080/
export HTTPS_PROXY=https://your.organization.proxy:8443/
export NO_PROXY=$(hostname),localhost,127.0.0.1,.internal.your.organization.com,</code></pre></li>
<li><p><strong><em>Bonus tip</em> (relevant to <em>any</em> system
migration, not just WSL):</strong> I <em>personally</em> like (and
recommend) to manage one configuration file for my Linux devices. A
common practice is to keep it <a
href="https://codeberg.org/maze/dotfiles">backed up on a Git
server</a>.<br> I call this file my <code>~/.userrc</code> (replace
“user” with <em>your name</em> or <em>initials</em>), and I source it
from the main <code>~/.bashrc</code> file by adding the line
<code>[ -f ~/.userrc ] &amp;&amp; . ~/.userrc</code> (which sources it,
only <strong>if</strong> it exists). The actual contents of your
personal configuration file is up to you, here is an example of
mine:</p>
<pre><code># this file should be sourced by adding the following expression to ~/.bashrc:
#   [ -f ~/.mzrc ] &amp;&amp; . ~/.mzrc

# env
export EDITOR=vi
export VISUAL=vim
export AWS_PROFILE=${AWS_PROFILE}  # keep same aws profile as parent shell

# aliases
alias ll=&#39;ls -hlF --group-directories-first --color=auto&#39;
alias diff=&#39;diff --color=auto&#39;
alias whatismyip=&#39;curl ifconfig.me&#39;
alias cat=&#39;bat -pp&#39;  # get bat from https://github.com/sharkdp/bat/releases/latest
alias k=kubectl

# completion
source &lt;(helm     completion bash)
source &lt;(kubectl  completion bash)
complete -F __start_kubectl k
complete -C aws_completer   aws
complete -C terraform       terraform</code></pre></li>
</ul>
<h2 id="additional-info">Additional info</h2>
<ul>
<li>Some useful WSL commands (for Powershell) - especially if you’ve got
more than one Linux distribution:
<ul>
<li><code>wsl --list --verbose</code> lists distros, their states, and
VM version (make sure your default version is 2).</li>
<li><code>wsl --distribution &lt;distro&gt;</code> runs a specific
distro (else will run the default one).</li>
<li><code>wsl --terminate &lt;distro&gt;</code> shuts down/restarts the
distro.</li>
<li><code>wsl --unregister &lt;distro&gt;</code> removes a distro from
WSL.</li>
</ul></li>
<li>Full and <a
href="https://docs.microsoft.com/en-us/windows/wsl/">official WSL
documentation</a>.</li>
</ul>
<h2 id="conclusion">Conclusion</h2>
<p>As much as we (or at least, I) strive to be at home and in our Linux
systems, sometimes we are forced to work in slightly less “ideal”
conditions...</p>
<p>If you need to work with a Linux system from within a Window host, I
recommend using WSL. It is as close to a native Linux experience as one
can get, from within Windows, without coming on expense of other
criteria such as performance.</p>
<p>WSL is one of the best ways I’ve found for achieving this.</p>

      <hr class="hidden-on-normal-displays">
    </main>

    <footer>
      <address>
        <ul id="social">
          <li><a rel="author" target="_blank" href="mailto:michael.zeevi@fastmail.net" title="E-mail: Michael Zeevi"           ><i class="fas fa-fw fa-envelope"   ></i><div class="hidden-on-tiny-displays"> E-mail</div></a></li>
          <li><a rel="author" target="_blank" href="https://fosstodon.org/@maze"       title="Mastodon: maze@fosstodon.org"    ><i class="fab fa-fw fa-mastodon"   ></i><div class="hidden-on-tiny-displays"> Mastodon</div></a></li>
          <li><a rel="author" target="_blank" href="https://pixelfed.social/@maze88"   title="Pixelfed: maze88@pixelfed.social"><i class="fas fa-fw fa-camera"     ></i><div class="hidden-on-tiny-displays"> Pixelfed</div></a></li>
          <li><a rel="author" target="_blank" href="https://codeberg.org/maze"         title="Codeberg: maze"                  ><i class="fas fa-fw fa-code-branch"></i><div class="hidden-on-tiny-displays"> Codeberg</div></a></li>
          <li><a rel="author" target="_blank" href="res/pgp.asc"                       title="PGP: Public key block"           ><i class="fas fa-fw fa-fingerprint"></i><div class="hidden-on-tiny-displays"> PGP</div></a></li>
          <li><a rel="author" target="_blank" href="https://keyoxide.org/hkp/a04876190823b1cc383e882d2458479e16ef8831" title="Keyoxide"><i class="fas fa-fw fa-key"></i><div class="hidden-on-tiny-displays"> Keyoxide</div></a></li>
          <li><a rel="author" target="_blank" href="https://maze.omg.lol"              title="omg.lol page"                    ><i class="fas fa-fw fa-heart"      ></i><div class="hidden-on-tiny-displays"> omg.lol</div></a></li>
        </ul>
      </address>

      <p id="donate">
        If you like what you see, feel free to <a target="_blank" href="https://liberapay.com/maze/donate">donate via Liberapay</a>.
      </p>

      <p id="license" xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/">
        <a href="index.html" property="dct:title" rel="cc:attributionURL">maze88.dev</a> by
        <a target="_blank" href="mailto:michael.zeevi@fastmail.net" property="cc:attributionName" rel="cc:attributionURL dct:creator">Michael Zeevi</a> is licensed under
        <a target="_blank" href="http://creativecommons.org/licenses/by-sa/4.0"                   rel="license noopener noreferrer">CC BY-SA 4.0</a>.
      </p>
    </footer>
  </body>
</html>
